# Sonar Server:
Can be download from the [sonarqube official website](https://www.sonarqube.org/downloads/).

To launch the server, move to the below relative location and run the appropriate shell/batch file.

    <sonar_binary_location>/sonarqube-6.7.2/bin/



# Commands
**Run Server**


## Documentation links
https://docs.sonarqube.org/display/SONAR/Documentation
