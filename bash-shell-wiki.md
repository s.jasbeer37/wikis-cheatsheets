[Online bash manual](https://www.gnu.org/software/bash/manual/)


## Variable
- assignment with equal sign
- no space before or after the equal sign
- for values with special characters, use quotes
- to delete a varible, use unset command

#### Samples
  ```
  a=123       # correct assignment
  a = 1234    # incorrect assignment
  echo $a     # will print the value
  unset a     # will remove the value of variable
  ```


## Some inbuilt commands
  - #### time
    - shows the time taken by a commands
    - can be appended before any command to get the time take to execute the command
    ```
    jasbsing-macOS-1:~ jasbsing$ time
    real	0m0.000s
    user	0m0.000s
    sys	0m0.000s
    jasbsing-macOS-1:~ jasbsing$ time sleep 2
    real	0m2.011s
    user	0m0.001s
    sys	0m0.002s
    ```

  - #### sleep <n>
    - sleep for n seconds

  - #### compgen -k
    - lists all the keywords of the current bash
